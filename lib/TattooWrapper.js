"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TattooWrapper = void 0;
const ethers_1 = require("ethers");
const abi_1 = require("./contracts/abi");
const providers_1 = require("@ethersproject/providers");
class TattooWrapper {
    constructor(rpcURL, tattooContractAddress) {
        this.tattooContract = new ethers_1.Contract(tattooContractAddress, abi_1.TattooABI, new providers_1.JsonRpcProvider(rpcURL));
    }
    async getContent() {
        let content = '';
        const maxIndex = 6;
        const promises = [];
        for (let i = 0; i <= maxIndex; i++) {
            promises.push(this.tattooContract.getContent(1, i));
        }
        (await Promise.all(promises))
            .sort((o1, o2) => {
            const [, maxI1] = o1;
            const [, maxI2] = o2;
            return maxI1.sub(maxI2).toNumber();
        })
            .forEach(value => {
            const [data] = value;
            content += data;
        });
        return content;
    }
}
exports.TattooWrapper = TattooWrapper;
