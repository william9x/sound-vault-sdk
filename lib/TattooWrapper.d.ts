import { Tattoo } from './contracts';
export declare class TattooWrapper {
    tattooContract: Tattoo;
    constructor(rpcURL: string, tattooContractAddress: string);
    getContent(): Promise<string>;
}
