import {BigNumber, Contract} from 'ethers';
import {TattooABI} from './contracts/abi';
import {Tattoo} from './contracts';
import {JsonRpcProvider} from '@ethersproject/providers';

export class TattooWrapper {
  tattooContract: Tattoo;

  constructor(rpcURL: string, tattooContractAddress: string) {
    this.tattooContract = new Contract(
      tattooContractAddress,
      TattooABI,
      new JsonRpcProvider(rpcURL)
    ) as Tattoo;
  }

  async getContent(): Promise<string> {
    let content = '';
    const maxIndex = 6;

    const promises: Promise<[string, BigNumber]>[] = [];

    for (let i = 0; i <= maxIndex; i++) {
      promises.push(this.tattooContract.getContent(1, i));
    }

    (await Promise.all(promises))
      .sort((o1, o2) => {
        const [, maxI1] = o1;
        const [, maxI2] = o2;
        return maxI1.sub(maxI2).toNumber();
      })
      .forEach(value => {
        const [data] = value;
        content += data;
      });

    return content;
  }
}
